# Java beans

In Java Spring, a bean is an object that is managed by the Spring IoC (Inversion of Control) container. It is an instance of a class that is created, configured, and assembled by the Spring framework. Beans serve as the fundamental building blocks of a Spring application, representing the components and services that make up the application's functionality.

1. **Definition**: A bean is defined in the Spring configuration, which can be done using XML-based configuration or annotation-based configuration.

2. **Creation and Lifecycle**: The Spring container is responsible for creating and managing the lifecycle of beans. When the application starts, the container creates bean instances based on their definitions. The container also handles the initialization and destruction of beans, allowing you to execute custom logic during these phases.

3. **Dependency Injection**: One of the core features of Spring is dependency injection, which allows beans to be wired together by declaring their dependencies. Rather than creating dependencies within a class, they are provided (injected) from external sources, making the code more modular and flexible.

4. **Scopes**: Beans can have different scopes, which define the lifecycle and visibility of the bean instance. The most commonly used scopes are Singleton (a single instance shared across the application) and Prototype (a new instance created for each request).

5. **Configuration Options**: Spring provides various configuration options to define beans and their properties. XML-based configuration allows explicit bean definitions, while annotation-based configuration reduces the boilerplate code by using annotations such as \@Component, \@Service, and \@Repository to mark classes as beans.

6. **Bean Wiring**: Beans can be wired together using different approaches, such as constructor injection, setter injection, or field injection. Dependency relationships are established either through configuration metadata or annotations like \@Autowired.

7. **Role and Functionality**: Beans in Spring can represent a wide range of components, including services, data access objects, controllers, and more. They encapsulate the application's business logic and functionality, promoting modularity and separation of concerns.

## Annotations

Annotations in Java provide a way to associate metadata with program elements such as classes, methods, fields, or parameters. They allow you to add additional information, instructions, or behavior to your code that can be processed at compile-time, runtime, or by external tools and frameworks.

1. **Annotation Syntax**: Annotations are represented by the @ symbol followed by the annotation name. They can include optional attributes, which are specified within parentheses and can accept values.

2. **Built-in Annotations**: Java provides several built-in annotations that serve specific purposes. Some commonly used built-in annotations include:

- **\@Override**: Indicates that a method overrides a superclass method.
- **\@Deprecated**: Marks a program element as deprecated, indicating that it should no longer be used.
- **\@SuppressWarnings**: Suppresses compiler warnings for a specific program element.
- **\@FunctionalInterface**: Indicates that an interface is a functional interface and can be used with lambda expressions.

3. **Marker Annotations vs. Single-Value Annotations vs. Full Annotations**:

- **Marker Annotations**: These annotations have no attributes. They simply indicate the presence of a specific feature or behavior.
- **Single-Value Annotations**: These annotations have a single attribute and can be specified using the attribute name without explicitly mentioning the attribute name.
- **Full Annotations**: These annotations have multiple attributes and require explicit specification of the attribute name along with its value.
- **Meta-Annotations**: Meta-annotations are annotations that can be applied to other annotations. They provide information about how the annotated annotations should be processed. Some common meta-annotations include \@Target, \@Retention, \@Documented, and \@Inherited.

- **Custom Annotations**: In addition to the built-in annotations, you can define your own custom annotations. Custom annotations are created by defining an interface and annotating it with the \@Interface keyword. You can define attributes within the custom annotation, which can accept values and provide additional information.

**Annotations are not magic** - they can be used in different parts of the application lifecycle. Some are parsed during the building process, at application startup or during runtime. Some are just present as markers, making the code more readable but don't hold any functionality.

# Dependency Injection

Dependency Injection (DI) is a design pattern used in Java Spring to achieve loose coupling and manage dependencies between objects. It is a fundamental concept in the Spring framework that allows you to externalize the creation and management of an object's dependencies, rather than having the object create or find them itself.

In DI, the dependencies required by an object are "injected" into it from external sources, typically by a container or framework, such as the Spring IoC container. This allows for easier testing, flexibility, and modularity in your application.

1. **Dependency**: A dependency is when an object relies on another object to fulfill its responsibilities or provide a specific functionality. For example, a UserService might depend on a UserRepository to perform database operations.

2. **Inversion of Control (IoC)**: In traditional programming, objects are responsible for creating and managing their dependencies. With DI and IoC, the control is inverted, meaning that the responsibility for managing dependencies is moved to an external entity, often a DI container like the Spring framework.

3. **Types of Dependency Injection**:

- **Constructor** Injection: Dependencies are provided through a constructor. The necessary dependencies are passed as arguments when creating an object.
- **Setter Injection**: Dependencies are provided through setter methods. The object's properties are set by invoking appropriate setter methods.
- **Field Injection**: Dependencies are injected directly into the fields of an object, typically using annotations.
Advantages of Dependency Injection:

- **Loose Coupling**: Dependencies are decoupled from the objects using them, allowing for easier maintenance, testing, and replacement of components.
- **Flexibility**: Dependencies can be easily switched or reconfigured by changing the configuration in the DI container, without modifying the dependent object.
- **Testability**: Dependencies can be easily mocked or replaced during unit testing, allowing for more effective and isolated testing.
- **Modular Design**: Dependencies are explicitly declared, making it clear which objects collaborate and reducing hidden dependencies.
- **\@Autowired Annotation**: In Java Spring, the \@Autowired annotation is used to indicate dependencies that should be automatically resolved and injected by the Spring container. It can be applied to constructors, setter methods, and fields.

Example of Constructor Injection using \@Autowired:

```java
@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    // ...
}

```

In the above example, the UserService class declares a dependency on UserRepository through constructor injection. The @Autowired annotation on the constructor instructs the Spring container to inject an instance of UserRepository when creating a UserService bean.

# Properties

In Java Spring, the properties mechanism allows you to externalize configuration values and access them within your application. Instead of hardcoding configuration values directly in the code, you can store them in external property files and retrieve them dynamically at runtime. This mechanism provides flexibility and easy configuration management without modifying the application's source code.

1.  **Property Files**: Configuration values are typically stored in property files, which are simple text files with key-value pairs. Each line in the file represents a property, where the key and value are separated by an equal sign (=) or a colon (:). Property files can have different formats, such as .properties, .yml, or .xml, depending on the configuration style used.

2. **Property Sources**: Property files can be loaded from various sources, including the classpath, file system, or external locations. Spring provides different mechanisms to define and load property sources, such as using XML-based configuration, annotation-based configuration, or programmatically configuring a PropertySource.

3. **Property Placeholder**: To access the properties within your application, you can use property placeholders. Property placeholders are placeholders in the form of ${key} or #{key} that are replaced with the corresponding values from the property files. These placeholders can be used in various Spring artifacts, such as XML configuration files, Java annotations, or \@Value annotations.

4. **Property Injection**: Properties can be injected into Spring beans using the \@Value annotation. The \@Value annotation can be applied to fields, constructor parameters, or setter methods, allowing you to inject specific property values directly into your beans.

Example of using the properties mechanism in Java Spring:

application.properties:

```java
app.name=My Application
app.version=1.0.0
db.url=jdbc:mysql://localhost:3306/mydb
db.username=myuser
db.password=mypassword
```

UserService.java:

```java
@Service
public class UserService {
    @Value("${app.name}")
    private String appName;

    @Autowired
    private UserRepository userRepository;

    // ...
}
```

In the above example, the application.properties file contains several properties, including app.name and database connection details. The UserService class uses the \@Value annotation to inject the value of the app.name property into the appName field. The UserRepository is autowired as a dependency.

By using the properties mechanism, you can easily configure your application by modifying property files, without requiring changes to the source code. This makes it convenient to switch configurations between different environments (e.g., development, staging, production) or customize behavior without recompiling and redeploying the application.

Note: The actual loading and management of property files are handled by the Spring framework and can be further customized based on your application's requirements.

# Configuration properties

In Java Spring, Configuration Properties is a mechanism that allows you to bind external configuration values to Java objects. It simplifies the process of reading and managing configuration properties by providing type-safe and structured access to configuration values.

Here are the key points to understand about Configuration Properties in Java Spring:

1. **Configuration Properties Class**: To use Configuration Properties, you define a dedicated Java class to represent the configuration properties. This class contains fields or properties that correspond to the configuration keys. It is typically annotated with \@ConfigurationProperties to specify the prefix for the configuration keys it will bind to.

2. **Binding Configuration Properties**: The Spring framework automatically binds the configuration properties from external sources (such as property files, environment variables, or command-line arguments) to the fields or properties of the Configuration Properties class. It uses naming conventions and reflection to match the keys with the fields/properties.

3. **Configuration Sources**: Configuration properties can be sourced from various locations, including property files (e.g., application.properties), YAML files (e.g., application.yml), environment variables, command-line arguments, or other custom property sources.

4. **Property Validation and Defaults**: Configuration Properties can include validation rules and default values. You can use annotations such as \@NotNull, \@Min, \@Max, and \@Value to enforce validation constraints on the property values. Additionally, default values can be specified using the defaultValue attribute of the \@ConfigurationProperties annotation or by initializing the fields/properties with default values.

5. **Accessing Configuration Properties**: Once the configuration properties are bound to the Configuration Properties class, you can access them throughout your application by autowiring or injecting the Configuration Properties class as a dependency. Spring will automatically populate the fields or properties with the configured values.

Example of using Configuration Properties in Java Spring:

ConfigurationPropertiesExample.java:

```java
@Configuration
@ConfigurationProperties(prefix = "app")
public class ConfigurationPropertiesExample {
    private String name;
    private int version;

    // Getters and setters

    @Override
    public String toString() {
        return "ConfigurationPropertiesExample [name=" + name + ", version=" + version + "]";
    }
}
```
application.properties:
```
app.name=My Application
app.version=1
```

Application.java:

```java
@SpringBootApplication
public class Application implements CommandLineRunner {
    @Autowired
    private ConfigurationPropertiesExample config;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) {
        System.out.println(config);
    }
}
```
In the above example, the ConfigurationPropertiesExample class represents the configuration properties with name and version fields. The \@ConfigurationProperties annotation specifies the prefix app for the configuration keys. The values for name and version are read from the app.name and app.version keys in the application.properties file. The ConfigurationPropertiesExample bean is autowired in the Application class, and its properties are printed in the run method.

By using Configuration Properties, you can centralize and organize your configuration values, make them type-safe, and benefit from Spring's automatic binding and validation capabilities. It provides a convenient way to manage and access configuration properties across your application.

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

```java
@Component
@ConfigurationProperties(prefix = "app")
public class ConfigurationPropertiesExample {
    private String name;
    private int version;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "ConfigurationPropertiesExample [name=" + name + ", version=" + version + "]";
    }
}
```

# MVC controllers

 MVC (Model-View-Controller) controllers are a key component in the Java Spring framework that handle incoming HTTP requests, process them, and generate appropriate responses. They act as an intermediary between the client and the application's business logic, facilitating the separation of concerns and enabling the implementation of the MVC architectural pattern.

1. **Role of MVC Controllers**: Controllers are responsible for handling and processing HTTP requests, interpreting user input, invoking the appropriate business logic (Model), and generating a response to be sent back to the client. They act as the entry point for client requests and orchestrate the flow of data between the user interface (View) and the application's business logic (Model).

2. **Controller Class**: A controller is typically implemented as a Java class annotated with the \@Controller annotation. This annotation identifies the class as a controller component and enables the Spring framework to automatically detect and register it.

3. **Request Mapping**: Controllers define methods to handle specific HTTP requests using the \@RequestMapping annotation or its derived annotations, such as \@GetMapping, \@PostMapping, \@PutMapping, etc. These annotations associate a URL pattern or path with a controller method, indicating which requests should be routed to that method.

4. **Controller Method**: A controller method is a Java method within the controller class that is annotated with a request mapping annotation. This method performs the necessary processing for a specific HTTP request and typically returns a response, which can be a view name, a JSON object, or other types based on the application's requirements.

5. **Method Parameters**: Controller methods can accept various parameters to access and process request data. Some commonly used parameters include:

- **\@RequestParam**: Binds request parameters to method parameters.
- **\@PathVariable**: Extracts values from the URI path.
- **\@RequestBody**: Binds the request body to a method parameter.
- **\@RequestHeader**: Retrieves specific request headers.
6. Return Types: Controller methods can have different return types based on the desired response. Some common return types include:

- **String**: Returns the logical view name to be resolved by the ViewResolver.
- **ModelAndView**: Returns a ModelAndView object, which combines a view name with model data.
- **\@ResponseBody**: Directly returns the response body (e.g., JSON, XML) without a view.
- **ResponseEntity**: Provides more control over the response, including headers and status codes.

Example of an MVC Controller in Java Spring:

```java
@Controller
@RequestMapping("/products")
public class ProductController {
    
    @Autowired
    private ProductService productService;

    @GetMapping("/{id}")
    public String getProductById(@PathVariable("id") Long id, Model model) {
        Product product = productService.getProductById(id);
        model.addAttribute("product", product);
        return "product-details";
    }

    @PostMapping("/")
    public ResponseEntity<String> createProduct(@RequestBody ProductRequest productRequest) {
        // Create product logic
        productService.createProduct(productRequest);
        return ResponseEntity.ok("Product created successfully");
    }
}
```
# Database support

Spring provides comprehensive support for working with databases through its database integration modules. These modules simplify database operations, provide transaction management, and offer various techniques for interacting with databases. Here's an overview of Spring's database support and some examples:

1. **Database Configuration**:

Spring provides the DataSource interface to manage database connections. You can configure the data source using properties or programmatically using a DataSource implementation like BasicDataSource or HikariDataSource.

```java
@Configuration
public class DatabaseConfig {
    @Bean
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/mydatabase");
        dataSource.setUsername("username");
        dataSource.setPassword("password");
        return dataSource;
    }
}
```

2. **JDBC Template**:

Spring's JdbcTemplate provides a simplified way to perform database operations using JDBC. It handles resource management and exception handling, reducing boilerplate code.

```java
@Repository
public class MyRepository {
    private final JdbcTemplate jdbcTemplate;
    
    @Autowired
    public MyRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    public void saveData(String data) {
        String sql = "INSERT INTO my_table (column_name) VALUES (?)";
        jdbcTemplate.update(sql, data);
    }
}
```

3. **Object-Relational Mapping (ORM)**:

Spring supports various ORM frameworks like Hibernate, JPA (Java Persistence API), and MyBatis. These frameworks provide object-oriented abstractions over relational databases, simplifying data access.

Example with Hibernate:

```java
@Entity
@Table(name = "my_table")
public class MyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    // Other fields, getters, and setters
}

@Repository
public class MyRepository {
    private final EntityManager entityManager;
    
    @Autowired
    public MyRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    public void saveEntity(MyEntity entity) {
        entityManager.persist(entity);
    }
}
```

4. **Spring Data JPA**:

Spring Data JPA provides a higher-level abstraction over JPA. It offers a repository programming model, eliminating the need for writing custom repository implementations.
Example:

```java
public interface MyRepository extends JpaRepository<MyEntity, Long> {
    // Custom queries and methods
}
```

5. **Transaction Management**:

Spring provides transaction management capabilities, allowing you to define transactional boundaries and control the behavior of database transactions declaratively or programmatically.
Example:

```java
@Service
@Transactional
public class MyService {
    @Autowired
    private MyRepository myRepository;
    
    public void saveData(String data) {
        // Perform data-related operations
        myRepository.saveData(data);
    }
}
```

These examples highlight some of the database support options provided by Spring. By leveraging Spring's database integration modules, you can streamline database operations, improve code quality, and benefit from Spring's transaction management capabilities.






